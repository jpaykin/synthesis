module Synth(synthIO,module Terms) where

import Terms
import Oracle (genTerm)
import State (Examples,makeExamples,printExamples)
import Sat
import Eqrel (examplesToRel)
import Data.SBV.Bridge.CVC4 (cvc4)

-- synthIO exs (f,x,a) provides an interface for synthesis of a function f
--   which takes argument x of type a. 
synthIO :: Examples -> (String,String,SType) -> IO ()
synthIO exs (f,x,a) = do
    printExamples exs
    st <- makeExamples exs
    let r = examplesToRel exs
    Just t <- genTerm (f,Var x a) r cvc4 ([x],[f]) st
    putStrLn $ "\nSOLUTION: " ++ f ++ "(" ++ x ++ ") := " ++ show t
