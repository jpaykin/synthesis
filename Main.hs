import Terms
import Interpretation (ContextSpec)
import Oracle         (genTerm)
import State          (makeExamples)
import Sat
import Eqrel          (examplesToRel)
import Data.SBV.Bridge.CVC4 (cvc4)

ex1 = ("pred",0,0)
ex2 = ("pred",1,0)
ex3 = ("pred",2,1)

spec :: ContextSpec
spec = (["x"],["pred"])

x = Var "x" SNat
t1 = ("pred",Succ x, x)
t2 = ("pred",Succ x, Succ $ App "pred" x)

conc = "concat"
ls1 = Var "ls1" (SList SNat)
ls2 = Var "ls2" (SList SNat)
ex1' = (conc,Pair Nil Nil, Nil)
ex2' = (conc,Pair (Cons 2 Nil) Nil, Cons 2 Nil)
z = Var "z" (SPair (SList SNat) (SList SNat))
spec' = (["z"],[conc])

andd = "and"
b = Var "b" SBoolean
ex1'' = (andd,Pair TT TT,TT)
ex2'' = (andd,Pair TT FF,FF)
ex3'' = (andd,Pair FF TT,FF)
ex4'' = (andd,Pair FF FF,FF)
spec'' = (["b"],[andd])

times2 = "times2"
ex1''' = (times2,0,0)
ex2''' = (times2,1,2)
ex3''' = (times2,2,4)
spec''' = (["x"],[times2])

pair = "pair"
ex1_pair = (pair,TT,Pair TT TT)
t_pair = ("pair",b,Pair b b)


main1 :: IO ()
main1 = do
    st <- makeExamples [ex1_pair]
    s <- compileTheorem_givenExamples t_pair (["b"],["pair"]) st 
    writeFile "./output.smt2" s

main2 :: IO Bool
main2 = do
    st <- makeExamples [ex1,ex2,ex3]
    isTheoremWith_givenExamples t2 cvc4 (["x"],["pred"]) st

main3 :: IO ()
main3 = do
    let exs = [ex1,ex2,ex3]
--    let exs = [ex1',ex2']
--    let exs = [ex1'',ex2'',ex3'',ex4'']
--    let exs = [ex1''',ex2''',ex3''']
    st <- makeExamples exs
    let r = examplesToRel exs
    t <- genTerm ("pred",x) r cvc4 spec st
    putStrLn $ "\nSOLUTION: " ++ show t


main = main3

--  st <- makeExamples [(ex1,ex2),(ex1',ex2')]
--  check_defn_given_examples tm1 tm2 ctx st
--    st <- makeExamples [(ex1',ex2')]
--    st <- makeExamples [(ex1,ex2)]
--    check_defn_given_examples tm1' tm2' ctx' st
--    s <- compile_defn_given_examples tm1' tm2' ctx' st
--    writeFile "./output.smt" s


-- compile :: [String] -> Intro -> Intro -> Symbolic SVarContext -> State -> IO String
-- compile ls t1 t2 ctx st = do
--     ls <- examples_to_SBools st ctx
--     compileToSMTLib True False $ do
--         ctx
--         importAxioms
--         constrain_all ls
--         forAll ls $ example_to_SBool_fun ls t1 t2 ctx

-- example_to_SBool_fun :: [String] -> Intro -> Intro -> Symbolic SVarContext -> Symbolic SBool
-- example_to_SBool_fun [] t1 t2 ctx = do
--     ctx' <- ctx
--     (st1,ctx1) <- intro_to_symbolic t1 ctx'
--     (st2,ctx2) <- intro_to_symbolic t2 ctx1
--     return $ st1 .== st2
-- example_to_SBool_fun (s:ls) t1 t2 ctx = forAll (s:ls) $ 


------------------------------------------------------------

-- (assert ; no quantifiers
--    (let ((s1 Zero))		  ; 0
--    (let ((s2 (f s1)))	  ; pred(0)
--    (let ((s3 (= s1 s2)))  ; 0=pred(0)
--    (let ((s4 (Succ s1)))  ; 1
--    (let ((s5 (f s4)))	  ; pred(1)
--    (let ((s6 (= s1 s5)))  ; 0=pred(1)
--    (let ((s7 (Succ s4)))  ; 2
--    (let ((s8 (f s7)))	  ; pred(2)
--    (let ((s9 (= s4 s8)))  ; 1=pred(2)
--    (let ((s10 (Succ s0))) ; S(x)
--    (let ((s11 (f s10)))	  ; pred(S(x))
--    (let ((s12 (= s0 s11))); x=pred(S(x))
--    (and s3 s6 s9 s_1 (not s12)))))))))))))))
-- 	;; 0=pred(0) /\ 0=pred(1) /\ 1=pred(2) /\ x<>pred(S(x))
