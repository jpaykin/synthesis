module State where

import Terms
import Interpretation 

import Control.Monad (liftM, liftM2)
import Data.IORef
import Data.Maybe (fromMaybe)
import Data.SBV.Bridge.CVC4
import Control.Concurrent.MVar

type Example  = (String,Intro,Intro)
type Examples = [Example]
type State    = (IORef (Examples, Int),MVar ())

newState :: IO State
newState = do
  lock <- newEmptyMVar
  ref <- newIORef ([],0)
  return (ref,lock)

showExample :: Example -> String
showExample (s,t1,t2) = s ++ "(" ++ show t1 ++ ") == " ++ show t2

showExamples :: Examples -> String
showExamples ls = "Examples:\n" ++ showExamples' ls
  where
    showExamples' [] = ""
    showExamples' (ex:ls) = "\t" ++ showExample ex ++ "\n" ++ showExamples' ls

printExamples :: Examples -> IO ()
printExamples ls = putStr $ showExamples ls

---------------------
-- Fresh Variables --
---------------------

-- getFresh x st gets a fresh variable xi where i is an integer
getFresh :: State -> IO String
getFresh (ref,_) = atomicModifyIORef' ref $ \(ex,i) -> ((ex,i+1),"x"++show i)

----------
-- Lock --
----------

acquireLock :: State -> IO ()
acquireLock (_,l) = putMVar l ()

releaseLock :: State -> IO ()
releaseLock (_,l) = takeMVar l

--------------------------------------
-- Accessing and Modifying Examples --
--------------------------------------

addExample :: State -> Example -> IO ()
addExample (ref,_) x = atomicModifyIORef' ref $ \ (ex,i) -> ((x:ex,i),())

makeExamples :: Examples -> IO State
makeExamples [] = newState
makeExamples (ex:ls) = do
  st <- makeExamples ls
  addExample st ex
  return st

getExamples :: State -> IO Examples
getExamples (ref,_) = do
    (ex,_) <- readIORef ref
    return ex



--------------------------
-- States and Symbolics --
--------------------------

-- ASSUMPTION: examples shouldn't have any term variables, so
-- the intro_to_symbolic operation shouldn't change the context at all.
example_to_SBool :: Example -> Context -> SBool
example_to_SBool (f,t1,t2) ctx = intro_to_symbolic  (App f t1) ctx 
                             .== intro_to_symbolic t2 ctx

example_to_negSBool :: Example -> Context -> SBool
example_to_negSBool (f,t1,t2) ctx = intro_to_symbolic  (App f t1) ctx 
                                ./= intro_to_symbolic t2 ctx

examples_to_SBools :: Examples -> Context -> [SBool]
examples_to_SBools ls ctx = map (\ x -> example_to_SBool x ctx) ls

