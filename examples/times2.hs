import Synth

ex1 = ("double",0,0)
ex2 = ("double",1,2)
ex3 = ("double",2,4)

main :: IO ()
main = synthIO [ex1,ex2,ex3] ("double","x",SNat)
