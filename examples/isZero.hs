import Synth

ex1 = ("isZero",0,TT)
ex2 = ("isZero",1,FF)

main = synthIO [ex1,ex2] ("isZero","x",SNat)
