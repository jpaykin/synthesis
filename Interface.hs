

oracle :: Chan Example -> Set Intro -> MVar Term -> State -> IO ()
oracle c seen res st = do
    ls <- getChanContext c
    forM_ ls $ \ (f,t1,t2) -> do
        putStrLn $ "\nIs " ++ showExample (f,t1,t2) ++ "?"
        s <- getLine
        evalStr s t2
    where
      evalStr s t2
        | s == "yes" || s == "y" = 
          if member t2 seen then return () 
          else tryPutMVar_ res $ intro_to_term t2
        | s == "Counterexample" = do
            putStrLn $ "Provide Counterexample:"
            s' <- getLine
            let ex' = stringToExample s'
            addExample st ex'
        | otherwise = return ()

stringToExample :: String -> Example
stringToExample = undefined

-- unification --


-- term generation --

genTerm :: (String,Intro) -> State -> IO Term
genTerm (f,t) res st = do
    c <- newChan
    res <- newEmptyMVar
    oracle c (S.singleton $ App f t) res st
    unifyCandidate c (f,t) 
    takeMVar res
    
    

