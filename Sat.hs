module Sat where

import Terms
import Interpretation
import State
import Axioms

import Data.SBV.Bridge.Z3

-----------------
-- Constraints --
-----------------

constrain_all :: [SBool] -> Symbolic ()
constrain_all [] = return ()
constrain_all (b:ls) = do
    constrain b
    constrain_all ls


-- Operation to restrict a term to be equal to the domain of one of the examples --

domain :: Examples -> [Intro]
domain = map (\ (f,t1,t2) -> t1)

restrictToSBools :: SBV Intro -> Examples -> Context -> [SBool]
restrictToSBools t ls ctx =
    map tEqual (domain ls) where
        tEqual t' = t .== intro_to_symbolic t' ctx

constrainToDomain :: Intro -> Examples -> Context -> Symbolic ()
constrainToDomain t exs ctx = constrain . bOr $
                              restrictToSBools (intro_to_symbolic t ctx) exs ctx

constrainExamples :: Examples -> Context -> Symbolic ()
constrainExamples exs ctx = constrain_all $ examples_to_SBools exs ctx

---------------------
-- Prover Commands --
---------------------

initConstructors :: Context -> Symbolic ()
initConstructors ctx = constrain $ s .== s
  where
    t = Pair (Cons TT (Cons FF Nil)) (Succ Zero) -- t uses all constructors
    s = intro_to_symbolic t ctx
    

proveConfiguration :: Example -> Examples -> ContextSpec -> Symbolic SBool
proveConfiguration (f,t1,t2) exs spec = do
    ctx <- makeContext spec                 -- initialize the context
    initConstructors ctx
    importAxioms                            -- get axioms about the constructors
    constrainExamples exs ctx               -- constrain the examples
    constrainToDomain t1 exs ctx            -- constrain t1 to the domain of the examples
    return $ example_to_SBool (f,t1,t2) ctx -- prove f(t1)=t2
    

proveWith_givenExamples :: Example -> SMTConfig -> ContextSpec -> State -> IO ThmResult
proveWith_givenExamples ex conf spec st = do
    exs <- getExamples st
    proveWith conf (proveConfiguration ex exs spec)

compileTheorem_givenExamples :: Example -> ContextSpec -> State -> IO String
compileTheorem_givenExamples ex spec st = do
    exs <- getExamples st
    -- first argument: SMT-Lib2
    -- second argument: for Prove Query
    compileToSMTLib True False (proveConfiguration ex exs spec)


isTheoremWith_givenExamples :: Example -> SMTConfig -> ContextSpec -> State -> IO Bool
isTheoremWith_givenExamples ex conf spec st = do
    r <- proveWith_givenExamples ex conf spec st
    return $ thmResultToBool r


-- Theorem Results --

thmResultToBool :: ThmResult -> Bool
thmResultToBool (ThmResult r) = not (smtResultToBool r)

smtResultToBool :: SMTResult -> Bool
smtResultToBool (Unsatisfiable _) = False
smtResultToBool (Satisfiable _ _) = True
smtResultToBool (Unknown _ _)     = True -- assume: probably satisfiable
smtResultToBool (TimeOut _)       = True -- assume: probably satisfiable
smtResultToBool (ProofError _ _)  = error "Proof error in SAT solver."



-- SMT Configurations --

cvc4_debug :: SMTConfig
cvc4_debug = cvc4 {verbose  = True, 
                  useLogic = Just . PredefinedLogic $ AUFLIA,
                  timeOut  = Just 10
                  }

z3_debug :: SMTConfig
z3_debug = z3 {verbose  = True, 
               useLogic = Just . PredefinedLogic $ AUFLIA,
               timeOut  = Just 10
               }
