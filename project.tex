\documentclass{article}
\usepackage{amsmath,amssymb}
\usepackage{alltt} % verbatim
\usepackage{fullpage}
\usepackage{tikz}
\usetikzlibrary{fit,arrows,calc,positioning}
\tikzstyle{arrow} = [draw,-latex',thick]
\tikzstyle{line} = [draw,thick]

\title{Programming by Example using SMT Solvers}
\author{Jennifer Paykin}
\date{CIS 673, Fall 2014}

\newcommand\TT{\texttt{TT}}
\newcommand\FF{\texttt{FF}}
\newcommand\TIf[3]{\texttt{if } #1 \texttt{ then } #2 \texttt{ else } #3}
\newcommand\Zero{0}
\newcommand\Succ[1]{\texttt{S} #1}
\newcommand\NCase[4]{\texttt{case } #1\texttt{ of }\{0 \Rightarrow #2 \mid S #3 \Rightarrow #4\}}
\newcommand\Nil{[]}
\newcommand\Cons[2]{#1 :: #2}
\newcommand\LCase[5]{\texttt{case } #1\texttt{ of }\{[] \Rightarrow #2 \mid #3::#4 \Rightarrow #5\}}
\newcommand\PCase[4]{\texttt{case } #1\texttt{ of } (#2,#3) \Rightarrow #4}

\renewcommand\times{\texttt{double}}
\newcommand\pred{\texttt{pred}}
\newcommand\plus{\texttt{plus}}
\newcommand\length{\texttt{len}}
\newcommand\even{\texttt{even}}

\newcommand\Term{\texttt{Term}~}

\begin{document}
\maketitle

\section{Introduction}

In this report I will describe a divide and conquer approach to programming by example,
a synthesis problem in which a program is generated from a small number of concrete examples.
Gulwani~\cite{gulwani12} points out that programming by example systems have two main considerations:
a user interaction model and an efficient search algorithm to generate candidates.
The approach presented here uses a fragment-based user interaction model, 
in which program fragments are presented to the user to accept or reject as a correct
program definition. The program fragments are of the form
\[ \length(\Cons x {ls}) = \Succ{(\length(ls))} \qquad\text{or}\qquad \plus(\Zero,y) = y \]
and may be accepted, rejected, or refuted by the user.

For this choice of user interaction model, we describe a search algorithm that 
generates program fragments from examples, with an emphasis on recursive calls.
Furthermore, because we generate only program \emph{fragments}, we describe a method
to check the consistency of a fragment with the set of examples.
This problem is trivial in the traditional programming by example problem 
because when generating complete programs, one just evaluates candidate programs
on the examples. The solution described here uses an SMT solver to check consistency.

A diagram of the programming by example system is given in Figure~\ref{fig:diagram}.


\begin{figure}
\begin{tikzpicture}[thick,
       boxed/.style={rectangle,draw,minimum height=2cm,minimum width=3cm},
       small/.style={rectangle,draw,minimum height=1cm,minimum width=0cm}
    ]
    \node[boxed] (S) at (00,0.0) {Search Algorithm};
    \node[small] (C) at (06,0.5) {Consistency Checker};
    \node[boxed] (O) at (12,0.0) {Oracle};
    \node        (E) at (03,2.0) {Examples};
    \node        ()  at (03,0.7) {candidate};
    \node        ()  at ( 9,0.7) {candidate};
    \node        ()  at ( 6,-.8) {accept/reject/counterexample};
%   \node        ()  at (7.3,-.25)   {counterexample};
    \node        ()  at (-.5,-1.5) {Success};
    \node        ()  at (2,-1.5) {Failure};


    \path [arrow] (S.18)  -- (C);
    \path [arrow] (C)     -- (O.162);
    \path [arrow] (O.198) -- (S.342);
    \path [arrow] (O.198) -| (C);
    \path [arrow] (E)     -| (S);
    \path [arrow] (E)     -| (C);
    \path [arrow] (S.220) -- (-1.2,-2);
    \path [arrow] (S.320) -- ( 1.2,-2);
\end{tikzpicture}
\caption{\label{fig:diagram} Synthesis Overview}
\end{figure}

Finally, we describe an implementation of the algorithm in Haskell,
as well as preliminary results in generating two example programs.

As a running example we will try to generate a function $\times$ that takes
a natural number $n$ to its double $2n$. We provide the following examples as input:
\[ \times(0)=0 \qquad \times(1)=2 \qquad \times(2)=4 \]
We will aim to generate the following function:
\[ \times(x) = \NCase x 0 {x'} {\Succ{\Succ{(\times(x'))}}}.\]


\section{Verification Oracle}   

The oracle in Figure~\ref{fig:diagram} interacts with the user to determine whether a
candidate program fragment is acceptable to be inserted into the larger program.
Different programming by example systems employ different approaches. 
The motivation behind synthesizing program fragments is that it is easier for a
human to manually verify the correctness of programs without case statements, rather than
complex definitions

When presented with a program fragment, the user has the option to accept,
reject or provide a counterexample. An example of the interaction in the 
Haskell implementation would be
\[ \text{Is } \times(x) == \Succ{\Succ{(\times(x))}} ? \]

The implementation currently presents candidates to the user in the order
they are generated, but one could easily implement a heuristic that
presents stronger candidates first. Other factors the interface should take
into account include
\begin{enumerate}
    \item Has this exact candidate been presented to the user already?
    \item Has a candidate for this fragment already been accepted?
    \item Is this candidate structurally non-decreasing (potentially
          leading to a non-terminating program)?
    \item Is this candidate sufficiently general (not overly tailored to the examples)?
\end{enumerate}
The Haskell implementation currently addresses point 2, and partially addresses points 1 and 3.

\section{Compatibility with Examples}

Because we are generating program fragments instead of entire programs, we need
some way of checking whether these candidates are compatible with the
concrete examples given by the user. In this section, consider two conflicting candidate fragments:
\[ \times(\Succ(x))=\Succ\Succ(x) \qquad\text{and}\qquad \times(\Succ(x))=\Succ\Succ(\times(x)) \]

\paragraph{A satisfiability query.}
Our intuition tells us that the candidate on the right is compatible with the examples, 
but the candidate on the left is not, because when $x=1$,
$\times(\Succ(x))=\times(2)=4 \neq \Succ\Succ(x)$. In English we can state this condition 
as a validity query:
\begin{alltt}
    Does there exist a function \times such that
        \times(0)=0, \times(1)=2, \times(2)=4,
    and for every variable x',
        \times(\Succ{x'}) == \Succ{\Succ{x'}}?
\end{alltt}

How do we formulate this question as a satisfiability query?
To begin with, we must eliminate the quantifiers exists and forall.
In addition, we must treat
all unbound variables as implicit existentials.
To eliminate the universal quantifier, we negate the conclusion and the result:
\begin{alltt}
    Is the following formula unsatisfiable?
        \times(0)=0, \times(1)=2, \times(2)=4,
    and
        \times(\Succ(x')) /= \Succ{\Succ{(x')}}
\end{alltt}
As expected, this formula is satisfiable with $x'=1$, which means that the
candidate expression is incompatible with the examples.

When we instantiate this query with the second candidate however,
we again the answer satisfiable with the following model:
$x'=2$ and $\times$ is any function mapping $3$ to $0$.
The query must respect the fact that we only know the behavior 
of $\times$ on the input examples. Thus we structure the query as follows:
\begin{alltt}
    Is the following formula unsatisfiable?
        \times(0)=0, \times(1)=2, \times(2)=4,
    and
        (x'=0 or x'=1 or x'=2),
    and
        \times(\Succ(x')) /= \Succ{\Succ{(\times(x'))}}
\end{alltt}
The formula is now unsatisfiable, which means that the given candidate
is compatible with the examples.

\paragraph{Representing terms.}
Next we will present this query in the language SMT-LIB2, a standardized
language for representing satisfiability queries to multiple solvers,
including z3 and cvc4~\cite{SMTLIB}. SMT-LIB2 is a multi-sorted specification
language, which means that every uninterpreted function and constant must be
given a type. We declare an uninterpreted sort \Term that will be
the domain of our introduction terms.
\begin{alltt}
    (declare-sort \Term 0)
\end{alltt}
The variable $x$ and function $\times$ will be uninterpreted functions
over terms:
\begin{alltt}
    (declare-fun x () \Term)
    (declare-fun \times (\Term) \Term)
\end{alltt}
In order to talk about values of the language like $\Zero$ and $\Succ{}$,
we define the constructors to be uninterpreted functions that are
governed by axioms classifying their behavior.
\begin{alltt}
    (declare-fun \Zero () \Term)
    (declare-fun S (\Term) \Term)
    ;; Disjointness assertion: 0 /= S(x)
    (assert (forall ((x \Term)) (not (= \Zero (\Succ{x})))))
    ;; Injectivity assertion: if S(x)=S(y) then x=y
    (assert (forall ((x \Term) (y \Term)) (=> (= (\Succ{x}) (\Succ{y})) (= x y))))
\end{alltt}
Finally, we add assertions about the examples and domain of $x$, and state the actual query:
\begin{alltt}
    (assert (and (= (\times \Zero) \Zero) (= (\times (\Succ{\Zero})) (\Succ{(\Succ\Zero)})) ...))
    (assert (or  (= x \Zero) (= x (\Succ\Zero)) (= x (\Succ{(\Succ\Zero)}))))
    (assert (not (= (\times{(\Succ{x})}) (\Succ{(\Succ{(\times x)})}))))
    (check-sat)
\end{alltt}

\section{Generation Algorithm}

The function definitions we will be generating will be terms in the following
functional, first-order language:
\begin{align*} 
    t &::= x \mid f(t) \mid \TT \mid \FF \mid \TIf {t_1} {t_2} {t_3} \\
    &\mid  \Zero \mid \Succ t \mid \NCase {t_1} {t_2} x {t_3}  \\
    &\mid  \Nil \mid \Cons {t_1} {t_2} \mid \LCase {t_1} {t_2} x {xs} {t_3} \\
    &\mid  (t_1,t_2) \mid \PCase {t_1} {x_1} {x_2} {t_2} 
\end{align*}
The program fragment candidates will be generated from the eliminator-free subset of terms:
\begin{align*}
    s &::= x \mid f(s) \mid \TT \mid \FF \mid \Zero \mid \Succ s 
    \mid \Nil \mid \Cons {s_1} {s_2} \mid (s_1,s_2)
\end{align*}

The basis of the generation algorithm is an equivalence relation on introduction terms.
We start by equating the examples; in the case of the $\times$
function, the relation generated by the first two examples is:
\[
    \{0,\times(0)\} \quad \{1,\Succ(\times(0))\} \quad \{2,\times(1),\Succ(1),\Succ\Succ(\times(0))\}.
\]

To generate a fragment candidate for $\times(\Succ~x')$, the algorithm identifies
the variable $x'$ with an arbitrary equivalence class in the relation.
The candidates for the fragment $\times(\Succ~x')$ are then the elements of
its equivalence class.

For example, if we equate $x'$ with $0$, we obtain the following equivalence relation:
\begin{align*}
   \{&0,x',\times(0),\times(x')\} \\
    \{&1,\Succ(x'),\Succ(\times(0)),\Succ(\times(x'))\}  \\
    \{&2,\Succ(\Succ(x')),\Succ(\Succ(\times(0))),\Succ(\Succ(\times(x'))), \\
    &\times(1),\times(\Succ(x')),\times(\Succ(\times(0))),\times(\Succ(\times(x')))\}
\end{align*}
Eventually we find $\Succ(\Succ(\times(x')))$ in the equivalence class of $\times(\Succ(x'))$,
as expected.

It may be the case that all the candidates in the equivalence class are rejected
by either the SMT checker or the user-based oracle.
For example, when we try to generate the fragment $\times(x)$ in this manner,
there are no acceptable candidates. In this case we use the type of the variable $x$
(here, $x$ has type Nat) to perform a case analysis on the variable.
We obtain
\[ \times(x) = \NCase {x} {\times(0)} {x'} {\times(\Succ{(x')})} \]
for which we try to generate the smaller fragments.

In this way, we are restricting the domain of generated case analyses to input variables
instead of arbitrary terms. For example, we would never generate the following definition
for the \even~predicate:
\[ \even(x)=\NCase x \TT {x'} {\TIf {\even(x')} \FF \TT}. \]
However, the search algorithm \emph{could} be adapted to generating 
mutually recursive programs and helper functions, which would eliminate this discrepancy. 
That is, we could generate:
\begin{align*} 
    \even(x) &= \NCase x \TT {x'} {\texttt{not}(\even(x'))} \\
    \texttt{not}(y) &= \TIf y \FF \TT
\end{align*}

\section{Implementation}

The candidate generation algorithm is implemented in Haskell, using the library
SBV for interfacing with the SMT solver\footnote{http://leventerkok.github.io/sbv/}.
The implementation currently supports first-order functions whose
domain is either booleans or natural numbers. The codomain of the function can range over
arbitrary terms.

The SBV library uses a ``Symbolic'' monad to translate Haskell functions
into calls to an SMT-solver capable of reading SMT-LIB2 input. 
The function \texttt{isSatisfiable} takes in a symbolic boolean value
and returns a code for either satisfiable, unsatisfiable, unknown, or timed out
to indicate the result.
In the ``Symbolic Boolean'' type it is possible to embed uninterpreted functions
(of uninterpreted sort \Term) and the assertions described in Section~3.
The uninterpreted sort \Term is derived automatically from the regular
Haskell datatype for terms, which means we can translate arbitrary terms
into the symbolic version. These symbolic terms can then be automatically
compared by the symbolic equality operation~(\texttt{.===}).

The equivalence relation on terms, described in Section~4, is defined as a pair
of hash maps in Haskell. The first map in the pair maps terms to indices,
and the second maps indices to sets of disjoint terms representing equivalence classes.
The hash maps are lazy, and only non-trivial equivalence classes
are stored. In order to equate two terms in the equivalence relation,
we must perform a recursive ``closing'' operation that lifts equivalent
subterms under some term context. In many cases this operation does not
have a least fixed point; consider the predecessor function,
in which $0 \equiv \pred(0) \equiv \pred(\pred(0)) \equiv \cdots$.
In the implementation we simply
restrict the closing operation to one or two iterations, which seems to be
sufficient for our test cases. Other more intelligent techniques,
like ruling out multiple applications of the same function,
could be applied.

To use the synthesizer, we provide a single function \texttt{synthIO}, 
defined in the module Synth.hs, which takes as input a set of examples
and the name and domain type of the function being generated,
and provides an interface with the synthesis algorithm.
An example program synthesizing the $\times$ function is given in 
Figure~\ref{fig:prog}. The user interface is presented on the right,
with user-supplied input marked by \texttt{>>}.

\begin{figure}
\begin{minipage}{0.5\textwidth}
\begin{alltt}
import Synth

ex1 = ("double",0,0)
ex2 = ("double",1,2)
ex3 = ("double",2,4)

main = synthIO [ex1,ex2,ex3] 
               ("double","x",SNat)
\end{alltt}
\end{minipage} \begin{minipage}{0.5\textwidth}
\begin{alltt}
Examples:
    double(0) == 0
    double(S(0)) == S(S(0))
    double(S(S(0))) == S(S(S(S(0))))

Is double(0) == 0?
>> yes

Is double(S(x0)) == S(S(double(x0)))?
>> yes

SOLUTION: double(x) := 
    case x of (0 => 0 | S x0 => S(S(double(x0))))
\end{alltt}
\end{minipage}
\caption{Interface with the Haskell synthesizer. \label{fig:prog}}
\end{figure}

The Haskell implementation can be found as a git repository
at the following url: 
\[ \texttt{https://bitbucket.org/jpaykin/synthesis}. \]

\section{Conclusion}

The algorithm presented in this paper takes a different approach to programming by example
\cite{gulwani12,gulwani12spreadsheet}
by generating fragments of programs in order to allow for distributed or parallel
execution. The goal is to take advantage of multiple 
cores to effectively scale the time needed to synthesize large programs.
In order to independently test the compatibility of program fragments with a set of 
examples, we take clues from the syntax-guided synthesis paradigm \cite{alur13},
treating the examples as a first-degree verification condition.

In this work we describe the algorithm and a preliminary Haskell implementation
that can handle a subset of the target language.
Potential extensions to the current system include a more subtle user interface as described in
Section~2, support for mutually recursive definitions and helper functions, and support
for arbitrary inductive datatypes.

\bibliographystyle{plain}
\bibliography{project}

\end{document}
