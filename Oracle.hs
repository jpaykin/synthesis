module Oracle where

import Terms
import Interpretation
import Eqrel
import State
import Sat

import Data.SBV.Bridge.CVC4
import Control.Monad
import Control.Parallel
import Control.Concurrent
import Control.Concurrent.MVar

killThreads :: [ThreadId] -> IO ()
killThreads [] = return ()
killThreads (id:ls) = do
    killThread id
    killThreads ls

exIsEqual :: Example -> Bool
exIsEqual (f,t1,t2) = App f t1 == t2

checkCandidate :: Example -> MVar Term -> SMTConfig -> ContextSpec -> State -> IO ()
checkCandidate ex res conf spec st = do
--    acquireLock st
--    putStrLn $ "Checking: " ++ show ex
--    releaseLock st
    b1 <- isTheoremWith_givenExamples ex conf spec st
    b2 <- isEmptyMVar res
    b3 <- return $ not $ exIsEqual ex
    if b1 && b2 && b3 then oracle ex res spec st else return ()

oracle :: Example -> MVar Term -> ContextSpec -> State -> IO ()
oracle (f,t1,t2) res spec st = do
    acquireLock st
    putStrLn $ "\nIs " ++ showExample (f,t1,t2) ++ "?"
    s <- getLine
    evalStr s 
    where -- make sure the lock gets released!
      evalStr s 
        | s == "yes" || s == "y" = do
            releaseLock st
            tryPutMVar_ res $ intro_to_term t2
        | s == "Counterexample" = do
            putStrLn $ "Provide Counterexample:"
            s' <- getLine
            let ex' = stringToExample s'
            releaseLock st
            addExample st ex'
        | otherwise = releaseLock st

stringToExample :: String -> Example
stringToExample = undefined
            


---------------------
-- Term Generation --
---------------------

-- eta expansion of the decomposition
expand :: Decomposition -> String -> EqRel Intro -> MVar Term 
       -> SMTConfig -> ContextSpec -> State -> IO ()
expand (x,SBoolean,c) f r res conf ctx st = do
    b <- isEmptyMVar res
    if not b then return () else do
    tTT <- genTerm (f,c TT) r conf ctx st
    tFF <- genTerm (f,c FF) r conf ctx st
    let mterm = liftM2 (\t1 t2 -> TIf (TVar x) t1 t2) tTT tFF
    tryPutMaybeMVar_ res mterm
expand (x,SNat,c) f r res conf ctx st = do
    b <- isEmptyMVar res
    if not b then return () else do
    tZero <- genTerm (f,c Zero) r conf ctx st
    y <- getFresh st
    let ctx' = updateVarContextSpec y ctx
    tSucc <- genTerm (f,c.Succ $ Var y SNat) r conf ctx' st
    let mterm = liftM2 (\t1 t2 -> NCase (TVar x) t1 y t2) tZero tSucc
    tryPutMaybeMVar_ res mterm
expand (x,SList a,c) f r res conf ctx st = do
    b <- isEmptyMVar res
    if not b then return () else do
    tNil <- genTerm (f,c Nil) r conf ctx st
    y1 <- getFresh st
    y2 <- getFresh st
    let ctx' = updateVarContextSpec y1 $ updateVarContextSpec y2 ctx
    let s = Cons (Var y1 a) (Var y2 $ SList a)
    tCons <- genTerm (f,c s) r conf ctx' st
    let mterm = liftM2 (\t1 t2 -> LCase (TVar x) t1 y1 y2 t2) tNil tCons
    tryPutMaybeMVar_ res mterm
expand (x,SPair a1 a2,c) f r res conf ctx st = do
    b <- isEmptyMVar res
    if not b then return () else do
    y1 <- getFresh st
    y2 <- getFresh st
    let ctx' = updateVarContextSpec y1 $ updateVarContextSpec y2 ctx
    let s = Pair (Var y1 a1) (Var y2 a2)
    tPair <- genTerm (f,c s) r conf ctx' st
    let mterm = liftM (\t' -> PCase (TVar x) y1 y2 t') tPair
    tryPutMaybeMVar_ res mterm
expand (x,SArrow a1 a2,c) f r res conf ctx st = do
    b <- isEmptyMVar res
    if not b then return () else do
    y <- getFresh st
    let ctx' = updateVarContextSpec y ctx
    error "Can't expand an arrow type"

tryPutMaybeMVar_ :: MVar Term -> Maybe Term -> IO ()
tryPutMaybeMVar_ res (Just t) = tryPutMVar_ res t
tryPutMaybeMVar_ _ _ = return ()


unifyCandidate :: (String,Intro) -> EqRel Intro -> MVar Term
               -> SMTConfig -> ContextSpec -> State -> IO ()
unifyCandidate (f,t) r res conf ctx st = do
    ex <- getExamples st
    let rs = unify (f,t) r ex
    forM_ rs checkCandidates  
  where
    checkCandidates :: EqRel Intro -> IO ()
    checkCandidates r' = do
        let cls = computeClass (App f t) r'
--        acquireLock st
--        putStrLn $ "Equivalence Relation: " ++ showEqRel r'
--        putStrLn $ "Class of " ++ f ++ "(" ++ show t ++ "): " ++ showSet show cls ++ "\n"
--        releaseLock st
        forS_ cls $ \ s -> checkCandidate (f,t,s) res conf ctx st


unify :: (String,Intro) -> EqRel Intro -> Examples -> [EqRel Intro]
unify (f,t) r ex = foldl (\ ls d -> ls ++ unifyExamples d) [r] $ decompose t where
    unifyExamples :: Decomposition -> [EqRel Intro]
    unifyExamples d = foldl (\ls e -> ls ++ unifyExample d e) [] $ ex
    unifyExample :: Decomposition -> Example -> [EqRel Intro]
    unifyExample (x,a,c) (f',s1,s2) = 
        fromFound (compatible x (App f t) (App f' s1)) [] $ \ s -> [equate (Var x a) s r]

    

branch :: (String,Intro) -> EqRel Intro -> MVar Term 
       -> SMTConfig -> ContextSpec -> State -> IO ()
branch (f,t) r res conf ctx st = do
--  acquireLock st
--  putStrLn $ "Branching on decompositions " ++ showDecompositions (decompose t) ++ " of " ++ show t
--  releaseLock st
  forM_ (decompose t) $ \ d -> expand d f r res conf ctx st
  
-------------------
-- Compatibility -- 
-------------------

data Compatibility = Found Intro | Compat | Uncompat

-- compatible x t1 t2 is:
--      Compat      if t2 is an instance of t1 where all variables are replaced by holes
--      Found(t')   if t2 is an instance of t1 where x occurs exactly once 
--                  and is replaced by t', and all other variables are replaced by holes
--      Uncompat    otherwise
compatible :: String -> Intro -> Intro -> Compatibility
compatible x (Var y a)        t               = if x==y then Found t else Compat
compatible x (App f1 t1)      (App f2 t2)     = if f1 == f2 then compatible x t1 t2 else Uncompat
compatible x TT               TT              = Compat
compatible x FF               FF              = Compat
compatible x Zero             Zero            = Compat
compatible x (Succ t1)        (Succ t2)       = compatible x t1 t2
compatible x Nil              Nil             = Compat
compatible x (Cons t11 t12)   (Cons t21 t22)  = exactOneFound (compatible x t11 t12) (compatible x t21 t22)
compatible x (Pair t11 t12)   (Pair t21 t22)  = exactOneFound (compatible x t11 t12) (compatible x t21 t22)
compatible _ _                _               = Uncompat

exactOneFound :: Compatibility -> Compatibility -> Compatibility
exactOneFound (Found t) Compat      = Found t
exactOneFound Compat    (Found t)   = Found t
exactOneFound Compat    Compat      = Compat
exactOneFound _         _           = Uncompat

fromFound :: Compatibility -> b -> (Intro -> b) -> b
fromFound (Found t) _ f = f t
fromFound _         x _ = x




---------------------
-- Term Generation --
---------------------

genTerm :: (String,Intro) -> EqRel Intro -> SMTConfig -> ContextSpec -> State -> IO (Maybe Term)
genTerm (f,t) r conf ctx st = do
  res <- newEmptyMVar
  unifyCandidate (f,t) r res conf ctx st
  branch (f,t) r res conf ctx st
  tryTakeMVar res 
    -- What if we have explored all branches? This will just hang,
    -- and hopefully be garbage collected eventually.
