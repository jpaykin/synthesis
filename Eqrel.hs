module Eqrel where

import Control.Parallel
import Control.Monad
import Data.Hashable
import qualified Data.HashMap.Lazy as H
import Data.Set (Set, union, singleton)
import qualified Data.Set as S
import Terms
import State
import qualified Data.Number.Nat as N
import Data.Maybe

-- Set operations

map2 :: (Ord a,Ord b,Ord c) => (a -> b -> c) -> Set a -> Set b -> Set c
map2 f xs ys = S.fold g S.empty xs where
    g x s = s `union` S.map (f x) ys

forS_ :: Set a -> (a -> IO b) -> IO ()
forS_ xs f = S.fold g (return ()) xs where
--  g :: a -> IO () -> IO ()
    g x i = do
      b <- f x
      i

showSet :: (a -> String) -> Set a -> String
showSet g xs = "{" ++ s ++ ".}" where
    s = S.fold f "" xs
    f x s' = g x ++ ", " ++ s'

showEqRel :: (Show a, Ord a) => EqRel a -> String
showEqRel (r1,r2,i) = showSet (showSet show) $ S.fromList $ H.elems r2

---------------------------
-- Equivalence relations --
---------------------------

type EqRel a = (H.HashMap a N.Nat, H.HashMap N.Nat (Set a), N.Nat)
instance Hashable N.Nat where hashWithSalt _ = N.fromNat 

emptyRel :: EqRel a
emptyRel = (H.empty,H.empty,0)

getFreshIndex :: EqRel a -> (N.Nat,EqRel a)
getFreshIndex (r1,r2,n) = (n,(r1,r2,n+1))

getKeys :: EqRel a -> [N.Nat]
getKeys (r1,r2,n) = H.keys r2

examplesToRel :: Examples -> EqRel Intro
examplesToRel [] = emptyRel
examplesToRel ((f,t1,t2):ls) = equate (App f t1) t2 $ examplesToRel ls

-- insert --

insert :: (Eq a, Hashable a) => a -> EqRel a -> EqRel a
insert x (r1,r2,n) = (H.insert x n r1, H.insert n (singleton x) r2, n+1)

insertIfMissing :: Intro -> EqRel Intro -> EqRel Intro
insertIfMissing t r = updateBindings i r where
    (r1,r2,n) = r
    (i,n') = maybe (n,n+1) (\ i -> (i,n)) $ lookupIntro t r
    s = fromMaybe (digIn t r) $ lookupId i r


-- update --

updateId :: Intro -> N.Nat -> EqRel Intro -> EqRel Intro
updateId t i (r1,r2,n) = (r1',r2,n) where
    r1' = H.insert t i r1

updateClass :: N.Nat -> Set Intro -> EqRel Intro -> EqRel Intro
updateClass i s (r1,r2,n) = (r1,r2',n) where
    r2' = H.insert i s r2

-- update the bindings of a particular equivalence class
-- by mapping all the members of the set to the index
updateBindings :: N.Nat -> EqRel Intro -> EqRel Intro
updateBindings i r = S.fold f r $ lookupId' i r where
    f t r' = updateId t i r'


-- lookup -- 

lookupIntro :: Intro -> EqRel Intro -> Maybe N.Nat
lookupIntro t (r1,_,_) = H.lookup t r1

lookupIntro' :: Intro -> EqRel Intro -> (N.Nat,EqRel Intro)
lookupIntro' t r = maybe (getFreshIndex r) (\ i -> (i,r)) $ lookupIntro t r

lookupId :: N.Nat -> EqRel Intro -> Maybe (Set Intro)
lookupId i (_,r2,_) = H.lookup i r2

lookupId' :: N.Nat -> EqRel Intro -> Set Intro
lookupId' i r = fromMaybe S.empty $ lookupId i r


lookupClass :: Intro -> EqRel Intro -> Set Intro
lookupClass t r = fromMaybe (singleton t) $ lookupId i r'
  where
    (i,r') = lookupIntro' t r
    
lookupIfMissing :: Intro -> EqRel Intro -> (Set Intro,EqRel Intro)
lookupIfMissing t r = (s,r') where
    (r1,r2,n) = r
    (i,n') = maybe (n,n+1) (\ i -> (i,n)) $ lookupIntro t r
    s = fromMaybe (digIn t r) $ lookupId i r
    r' = updateBindings i r

-- delete 
deleteIndex :: N.Nat -> EqRel Intro -> EqRel Intro
deleteIndex i (r1,r2,n) = (r1,H.delete i r2,n)

-- non-recursive combine
combine :: Intro -> Intro -> EqRel Intro -> EqRel Intro
combine t1 t2 r = if i1==i2 then r else r'''
  where
    (i1,r1) = lookupIntro' t1 r
    (i2,r2) = lookupIntro' t2 r1
    s1 = lookupClass t1 r2
    s2 = lookupClass t2 r2
    r' = updateClass i1 (s1 `union` s2) r2
    r'' = updateBindings i1 r'
    r''' = deleteIndex i2 r''   



-- combine t1 t2 (r1,r2,i) = maybe  (r1' i,r2' i,i+1) (\j -> (r1' j,r2' j,i)) m
--   where
--     m = combineIds (lookupIntro t1 (r1,r2,i)) (lookupIntro t2 (r1,r2,i))
--     r1' j = H.insert t1 j $ H.insert t2 j r1
--     r2' j = H.insert j (S.fromList [t1,t2]) r2 

combineIds :: Maybe N.Nat -> Maybe N.Nat -> Maybe N.Nat
combineIds (Just m) (Just n) = Just m
combineIds (Just m) _        = Just m
combineIds _        (Just n) = Just n
combineIds _        _        = Nothing

    -- let xs = lookup_class x r in
    -- let ys = lookup_class y r in
    -- let zs = union xs ys in
    -- -- for everything related to either x or y, they are now related to everything in zs
    -- let g x' r' = H.insert x' zs r' in
    -- S.fold g r zs 




combineOneSet :: Intro -> Set Intro -> EqRel Intro -> EqRel Intro
combineOneSet x xs r = S.fold (\ y r' -> combine x y r') r xs


combineSet :: Set Intro -> EqRel Intro -> EqRel Intro
combineSet xs r = S.fold (\ x r' -> combineOneSet x xs r') r xs


--

-- More indepth combine operation for terms --

closeIntro :: Intro -> EqRel Intro -> EqRel Intro
closeIntro t r = combineSet (digIn t r) r

closeIntrosOnce :: Set Intro -> EqRel Intro -> EqRel Intro
closeIntrosOnce ls r = S.fold closeIntro r ls

closeIntros :: N.Nat -> Set Intro -> EqRel Intro -> EqRel Intro
closeIntros 0 ls r = r
closeIntros n ls r = closeIntrosOnce ls $ closeIntros (n-1) ls r

closeId :: N.Nat -> EqRel Intro -> EqRel Intro
closeId i r = maybe r f $ lookupId i r where
    f s = closeIntrosOnce s r

closeIdList :: [N.Nat] -> EqRel Intro -> EqRel Intro
closeIdList ls r = foldl (\r' i -> closeId i r') r ls

closeIds :: N.Nat -> [N.Nat] -> EqRel Intro -> EqRel Intro
closeIds 0 ls r = r
closeIds n ls r = closeIdList ls $ closeIds (n-1) ls r

closeOnce :: EqRel Intro -> EqRel Intro
closeOnce r = foldl (\ r' i -> closeId i r') r $ getKeys r

close :: N.Nat -> EqRel Intro -> EqRel Intro
close 0 r = r
close n r = closeOnce $ close (n-1) r

equate :: Intro -> Intro -> EqRel Intro -> EqRel Intro
equate t1 t2 r = -- close 1 (combine t1 t2 r)
    let (i1,r1) = maybe (getFreshIndex r) (\i -> (i,r)) $ lookupIntro t1 r in
    let (i2,r2) = maybe (getFreshIndex r1) (\i -> (i,r1)) $ lookupIntro t2 r1 in
--    closeIds 2 [i1,i2] $ combine t1 t2 r2
    closeIds 1 (getKeys r) $ combine t1 t2 r2

computeClass :: Intro -> EqRel Intro -> Set Intro
computeClass = digIn

-- close a set and then integrate with context


----------------------------
-- Decomposition of Terms --
----------------------------
 
type Decomposition = (String, SType, Intro -> Intro)

showDecomposition :: Decomposition -> String
showDecomposition (x,a,f) = "\\ " ++ x ++ " -> " ++ show (f (Var x a))
showDecompositions :: [Decomposition] -> String
showDecompositions ls = "[" ++ f ls ++ "]" where
    f :: [Decomposition] -> String
    f []     = ""
    f [d]    = showDecomposition d
    f (d:ls) = showDecomposition d ++ ", " ++ f ls



decompose :: Intro -> [Decomposition]
decompose (Var x a) = [(x,a,id)]
decompose (App f t) = map g $ decompose t where
    g (x,a,c) = (x,a,\t -> App f $ c t)
decompose TT = []
decompose FF = []
decompose Zero = []
decompose (Succ t) = map g $ decompose t where
    g (x,a,c) = (x,a,Succ . c)
decompose Nil = []
decompose (Cons t1 t2) = ls1 ++ ls2 where
    ls1 = map g1 $ decompose t1
    ls2 = map g2 $ decompose t2
    g1 (x,a,c) = (x,a,\s->Cons (c s) t2)
    g2 (x,a,c) = (x,a,\s->Cons t1 (c s))
decompose (Pair t1 t2) = ls1 ++ ls2 where
    ls1 = map g1 $ decompose t1
    ls2 = map g2 $ decompose t2
    g1 (x,a,c) = (x,a,\s->Pair (c s) t2)
    g2 (x,a,c) = (x,a,\s->Pair t1 (c s))




-- Equivalence classes for terms with recursion


digIn :: Intro -> EqRel Intro -> Set Intro
digIn (App f t) r = S.fold (union_lookup (App f) r) S.empty $ 
                        introFilter (App f t) $ digIn t r
digIn t         r = S.fold (union_lookup id      r) S.empty $ digOut t r

digOut :: Intro -> EqRel Intro -> Set Intro
digOut (Succ t)     r = S.map Succ (digIn t  r)
digOut (Cons t1 t2) r = map2  Cons (digIn t1 r) (digIn t2 r)
digOut (Pair t1 t2) r = map2  Pair (digIn t1 r) (digIn t2 r)
digOut t            r = singleton t

union_lookup :: (Intro -> Intro) -> EqRel Intro -> Intro -> Set Intro -> Set Intro
union_lookup g r t s = s `union` lookupClass (g t) r


--get_class
--get_class t r = S.fold (\ t' ls -> ls `union` lookup_class t' r) (singleton t) $ dig t r

--foldl (\ ls s -> ls ++ dig s r) [] $ lookup_class t r where
    -- dig :: [Intro] -> Intro -> [Intro]
    -- dig ls s = ls ++ (foldl f [] $ decompose s)

    -- f :: [Intro] -> Decomposition -> [Intro]
    -- f ls (x,a,c) = ls ++ (foldl (\ls' s -> ls' ++ look (c s)) [] $ look $ Var x a)
--    look :: Intro -> [Intro]
--    look t' = lookup_class t' r


--foldl (\ ls t' -> ls ++ dig t' r) 
--                [] $ lookup_class t r

-- TODO: add parallelism
-- just go one level down, but remember.
-- dig :: Intro -> EqRel Intro -> Set Intro
-- dig (Succ t)      r = union ls1 ls2 where
--     ls1 = S.map Succ (lookup_class t r)
--     ls2 = S.map Succ (dig t r)
-- dig (Cons t1 t2)  r = ls1 `union` ls2 `union` ls3 `union` ls4 where
--     ls1 = map2  Cons (lookup_class t1 r) (lookup_class t2 r)
--     ls2 = map2  Cons (lookup_class t1 r) (dig t2 r)
--     ls3 = map2  Cons (dig t1 r)          (lookup_class t2 r)
--     ls4 = map2  Cons (dig t1 r)          (dig t2 r)
-- dig (Pair t1 t2)  r = ls1 `union` ls2 `union` ls3 `union` ls4 where
--     ls1 = map2  Pair (lookup_class t1 r) (lookup_class t2 r)
--     ls2 = map2  Pair (lookup_class t1 r) (dig t2 r)
--     ls3 = map2  Pair (dig t1 r)          (lookup_class t2 r)
--     ls4 = map2  Pair (dig t1 r)          (dig t2 r)
-- dig (App f t)     r = ls1 `union` ls2 where
--     ls1 = S.map (App f) (lookup_class t r)
--     ls2 = S.map (App f) (dig t r)
-- dig t             _ = S.empty



isSubterm :: Intro -> Intro -> Bool
isSubterm t1 t2 = t1 == t2 || isStrictSubterm t1 t2

isStrictSubterm :: Intro -> Intro -> Bool
isStrictSubterm t1 (Var _ _) = False
isStrictSubterm t1 (App f t2) = isSubterm t1 t2
isStrictSubterm t1 TT = False
isStrictSubterm t1 FF = False
isStrictSubterm t1 Zero = False
isStrictSubterm t1 (Succ t2) = isSubterm t1 t2
isStrictSubterm t1 Nil = False
isStrictSubterm t1 (Cons t21 t22) = isSubterm t1 t21 || isSubterm t1 t22
isStrictSubterm t1 (Pair t21 t22) = isSubterm t1 t21 || isSubterm t1 t22

notSubtermFilter :: Intro -> Set Intro -> Set Intro
notSubtermFilter t xs = S.filter (\ s -> not $ isSubterm t s) xs

hasNumApps :: String -> Intro -> N.Nat
hasNumApps f (App g t)      = if f==g then 1 + hasNumApps f t else hasNumApps f t
hasNumApps f (Succ t)       = hasNumApps f t
hasNumApps f (Cons t1 t2)   = hasNumApps f t1 + hasNumApps f t2
hasNumApps f (Pair t1 t2)   = hasNumApps f t1 + hasNumApps f t2
hasNumApps f _              = 0

introFilter :: Intro -> Set Intro -> Set Intro
introFilter t xs = S.filter (introFilterPred t) xs

introFilterPred :: Intro -> Intro -> Bool
introFilterPred (App f t) s    = (not $ isSubterm (App f t) s) && hasNumApps f s <= 1 && introFilterPred t s
introFilterPred (Succ t)  s    = introFilterPred t s 
introFilterPred (Cons t1 t2) s = introFilterPred t1 s && introFilterPred t2 s
introFilterPred (Pair t1 t2) s = introFilterPred t1 s && introFilterPred t2 s
introFilterPred t            s = True
