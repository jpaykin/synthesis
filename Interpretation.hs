module Interpretation where

import Terms

import Data.HashMap.Lazy as H
import Data.SBV.Bridge.Z3
import Data.Maybe (fromMaybe)

---------------------------------------------
-- Symbolic Interpretation of Intro / Elim --
---------------------------------------------

type SIntro = SBV Intro

type ContextSpec = ([String],[String])
type VarContext = HashMap String (SIntro)
type FunContext = HashMap String (SIntro -> SIntro)

data ContextData = F0 SIntro | F1 (SIntro -> SIntro) | F2 (SIntro -> SIntro -> SIntro)
type Context = HashMap String ContextData

emptyContext :: Context
emptyContext = H.empty

updateVarContext :: String -> SIntro -> Context -> Context
updateVarContext s x ctx = H.insert s (F0 x) ctx

updateFunContext :: String -> (SIntro -> SIntro) -> Context -> Context
updateFunContext s f ctx = H.insert s (F1 f) ctx

updateFunContext2 :: String -> (SIntro -> SIntro -> SIntro) -> Context -> Context
updateFunContext2 s f ctx = H.insert s (F2 f) ctx

updateContext :: String -> ContextData -> Context -> Context
updateContext s x ctx = H.insert s x ctx

initContext :: Context
initContext = 
  updateContext "TT"   (F0 uTT)   $
  updateContext "FF"   (F0 uFF)   $
  updateContext "Zero" (F0 uZero) $
  updateContext "Succ" (F1 uSucc) $
  updateContext "Nil"  (F0 uNil)  $
  updateContext "Cons" (F2 uCons) $
  updateContext "Pair" (F2 uPair) $
  emptyContext
  

makeContext :: ContextSpec -> Symbolic Context 
makeContext ([],      [])      = return initContext
makeContext ((s:ls1), ls2)     = do
  x <- free s
  ctx <- makeContext(ls1, ls2)
  return $ updateContext s (F0 x) ctx
makeContext (ls1,     (s:ls2)) = 
  let f = uninterpret "f" 
  in do
    ctx <- makeContext(ls1, ls2)
    return $ updateContext s (F1 f) ctx


isJustF0 :: String -> Maybe ContextData -> SIntro
isJustF0 s (Just (F0 t)) = t
isJustF0 s _ = error $ "Expected variable or constructor " ++ s ++ " undefined."

isJustF1 :: String -> Maybe ContextData -> SIntro -> SIntro
isJustF1 s (Just (F1 f)) = f
isJustF1 s _ = error $ "Expected variable or constructor " ++ s ++ " undefined."

isJustF2 :: String -> Maybe ContextData -> SIntro -> SIntro -> SIntro
isJustF2 s (Just (F2 f)) = f
isJustF2 s _ = error $ "Expected variable or constructor " ++ s ++ " undefined."


lookupF0 :: String -> Context -> SIntro
lookupF0 s ctx = isJustF0 s $ H.lookup s ctx

lookupF1 :: String -> Context -> (SIntro -> SIntro)
lookupF1 s ctx = isJustF1 s $ H.lookup s ctx

lookupF2 :: String -> Context -> (SIntro -> SIntro -> SIntro)
lookupF2 s ctx = isJustF2 s $ H.lookup s ctx
--fromMaybe err $ H.lookup s ctx where
--err = error $ "Expected function or constructor " ++ s ++ " undefined."

updateVarContextSpec :: String -> ContextSpec -> ContextSpec
updateVarContextSpec s (ls1,ls2) = (s:ls1,ls2)


-- Uninterpreted Constructors --

uTT :: SBV Intro
uTT = uninterpret "TT"

uFF :: SBV Intro
uFF = uninterpret "FF"

uZero :: SBV Intro
uZero = uninterpret "Zero"

uSucc :: SBV Intro -> SBV Intro
uSucc = uninterpret "Succ"

uNil :: SBV Intro
uNil = uninterpret "Nil"

uCons :: SBV Intro -> SBV Intro -> SBV Intro
uCons = uninterpret "Cons"

uPair :: SBV Intro -> SBV Intro -> SBV Intro
uPair = uninterpret "Pair"

-- Interpretation Functions --

intro_to_symbolic :: Intro -> Context -> SBV Intro
intro_to_symbolic (Var x a) ctx    = lookupF0 x ctx
intro_to_symbolic (App f t) ctx    = lookupF1 f ctx $ intro_to_symbolic t ctx
intro_to_symbolic TT ctx           = uTT
intro_to_symbolic FF ctx           = uFF
intro_to_symbolic Zero ctx         = uZero
intro_to_symbolic (Succ t) ctx     = uSucc (intro_to_symbolic t ctx)
intro_to_symbolic Nil ctx          = uNil
intro_to_symbolic (Cons t1 t2) ctx = uCons st1 st2 where
    st1 = intro_to_symbolic t1 ctx
    st2 = intro_to_symbolic t2 ctx
intro_to_symbolic (Pair t1 t2) ctx = uPair st1 st2 where
    st1 = intro_to_symbolic t1 ctx
    st2 = intro_to_symbolic t2 ctx

