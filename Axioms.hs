module Axioms where

import Control.Monad (liftM2, forM_)
import Data.SBV.Bridge.CVC4 hiding (sAssert)


constructors0 = ["TT","FF","Zero","Nil"]
constructors1 = ["Succ"]
constructors2 = ["Cons","Pair"]

-- SMT-lib shortcuts

sApply1 :: String -> String -> String
sApply1 f t     = "(" ++ f ++ " " ++ t ++ ")"
sApply2 f t1 t2 = "(" ++ f ++ " " ++ t1 ++ " " ++ t2 ++ ")"

sForall1 s = "(forall ((t1 Intro)) " ++ s ++ ")"
sForall2 s = "(forall ((t1 Intro) (t2 Intro)) " ++ s ++ ")"
sForall3 s = "(forall ((t1 Intro) (t2 Intro) (t3 Intro)) " ++ s ++ ")"
sForall4 s = "(forall ((t1 Intro) (t2 Intro) (t3 Intro) (t4 Intro)) " ++ s ++ ")"

sImplies :: String -> String -> String
sImplies s1 s2 = "(=> " ++ s1 ++ " " ++ s2 ++ ")"
sAnd     s1 s2 = "(and " ++ s1 ++ " " ++ s2 ++ ")"

sEq :: String -> String -> String
sEq s1 s2 = "(= " ++ s1 ++ " " ++ s2 ++ ")"

sNot :: String -> String
sNot s = "(not " ++ s ++ ")"

sAssert :: String -> String
sAssert s = "(assert " ++ s ++ ")"

-- Disjointness Axioms
axDisjoint00 :: String -> String -> String
axDisjoint00 x y = sNot $ sEq x y

axDisjoint01 x y = sForall1 $ axDisjoint00 x (sApply1 y "t1")
axDisjoint02 x y = sForall2 $ axDisjoint00 x (sApply2 y "t1" "t2")
axDisjoint11 x y = sForall2 $ axDisjoint00 (sApply1 x "t1") (sApply1 y "t2")
axDisjoint12 x y = sForall3 $ axDisjoint00 (sApply1 x "t1") (sApply2 y "t2" "t3")
axDisjoint22 x y = sForall4 $ axDisjoint00 (sApply2 x "t1" "t2") (sApply2 y "t3" "t4")

axDisjointAll :: [[String]]
axDisjointAll = 
  let f00 x y = if x==y then [] else [sAssert $ axDisjoint00 x y] in
  let f01 x y = [sAssert $ axDisjoint01 x y] in
  let f02 x y = [sAssert $ axDisjoint02 x y] in
  let f11 x y = if x==y then [] else [sAssert $ axDisjoint11 x y] in
  let f12 x y = [sAssert $ axDisjoint12 x y] in
  let f22 x y = if x==y then [] else [sAssert $ axDisjoint22 x y] in
  trim $
     liftM2 f00 constructors0 constructors0 
  ++ liftM2 f01 constructors0 constructors1
  ++ liftM2 f02 constructors0 constructors2
  ++ liftM2 f11 constructors1 constructors1
  ++ liftM2 f12 constructors1 constructors2
  ++ liftM2 f22 constructors2 constructors2
  where
    trim [] = []
    trim ([]:ls) = trim ls
    trim (x:ls)  = x:trim ls

-- Injective Axioms

axInjective1 c = sForall2 $ sImplies (sEq (sApply1 c "t1") (sApply1 c "t2")) (sEq "t1" "t2")
axInjective2 c = sForall4 $ sImplies (sEq (sApply2 c "t1" "t2") (sApply2 c "t3" "t4"))
                          $ sAnd (sEq "t1" "t3") (sEq "t2" "t4")

axInjectiveAll :: [[String]]
axInjectiveAll = 
  let ls1 = map (\s -> [sAssert $ axInjective1 s]) constructors1 in
  let ls2 = map (\s -> [sAssert $ axInjective2 s]) constructors2 in
  ls1 ++ ls2


-- Get the axioms

importAxioms :: Symbolic ()
--importAxioms = do
--    addAxiom "" [sAssert $ axDisjoint01 "Zero" "Succ"]
importAxioms = forM_ (axDisjointAll ++ axInjectiveAll) $ \ ls -> addAxiom "" ls

