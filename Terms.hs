{-# LANGUAGE DeriveDataTypeable #-}
module Terms where 

import Data.SBV.Bridge.Z3 
import Data.Generics

import Data.Hashable
import Control.Concurrent.MVar (MVar, tryPutMVar)
tryPutMVar_ :: MVar a -> a -> IO ()
tryPutMVar_ m a = do
  tryPutMVar m a
  return ()


-----------
-- TYPES --
-----------

data SType = SBoolean | SNat | SList SType | SPair SType SType | SArrow SType SType
  deriving (Eq,Show,Ord,Data,Typeable)

-----------
-- TERMS --
-----------

data Term = TVar String 
          | Ttt | Tff | TIf Term Term Term
          | TZero | TSucc Term | NCase Term Term String Term
          | TNil | TCons Term Term | LCase Term Term String String Term
          | TPair Term Term | PCase Term String String Term
          | TLambda String Term | TApp Term Term
  deriving (Eq, Ord, Data, Typeable)
instance HasKind Term 
instance SymWord Term

-- Instance for Hashable
instance Hashable Term where
  hashWithSalt i t = hashWithSalt i (show t)

-- Instance for Show
show_term :: Term -> String
show_term (TVar x)    = x
show_term Ttt         = "tt"
show_term Tff         = "ff"
show_term (TIf t1 t2 t3) = "if " ++ show_term t1
          ++ " then " ++ show_term t2 ++ " else " ++ show_term t3
show_term TZero          = "0"
show_term (TSucc t)      = "S(" ++ show_term t ++ ")"
show_term (NCase t1 t2 x t3) = "case " ++ show_term t1 
          ++ " of (0 => " ++ show_term t2 
          ++ " | S " ++ x ++ " => " ++ show_term t3 ++ ")"
show_term (TNil)       = "[]"
show_term (TCons t1 t2) = show_term t1 ++ "::" ++ show_term t2
show_term (LCase t1 t2 x xs t3) = "case " ++ show_term t1
          ++ " of ([] => " ++ show_term t2
          ++ " | " ++ x ++ "::" ++ xs ++ " => " ++ show_term t3 ++")"
show_term (TPair t1 t2) = "(" ++ show_term t1 ++ "," ++ show_term t2 ++ ")"
show_term (PCase t1 x y t2) = "case " ++ show_term t1
          ++ " of (" ++ x ++ "," ++ y ++ ") => " ++ show_term t2
show_term (TLambda x t) = "\\" ++ x ++ "." ++ show_term t
show_term (TApp t1 t2) = show_term t1 ++ "(" ++ show_term t2 ++ ")"

instance Show Term where
  show = show_term

------------------
-- INTRO / ELIM --
------------------

data Intro = Var String SType | App String Intro
           | TT   | FF
           | Zero | Succ Intro
           | Nil  | Cons Intro Intro
           | Pair Intro Intro
  deriving (Eq, Ord, Data, Typeable)
instance HasKind Intro
instance SymWord Intro


-- Instance for Show
instance Show Intro where
  show = show . intro_to_term

-- Instance for Hashable
instance Hashable Intro where
  hashWithSalt i t = hashWithSalt i (show t)

-- turn elim and intro forms into terms
intro_to_term :: Intro -> Term

intro_to_term (Var x s)     = TVar x
intro_to_term (App f t)     = TApp (TVar f) (intro_to_term t)
intro_to_term TT            = Ttt
intro_to_term FF            = Tff
intro_to_term Zero          = TZero
intro_to_term (Succ t)      = TSucc (intro_to_term t)
intro_to_term Nil           = TNil
intro_to_term (Cons t1 t2)  = TCons (intro_to_term t1) (intro_to_term t2)
intro_to_term (Pair t1 t2)  = TPair (intro_to_term t1) (intro_to_term t2)

-- Num instance
num_to_intro :: (Num a,Ord a) => a -> Intro
num_to_intro n | n <= 0 = Zero
               | otherwise = Succ . num_to_intro $ n-1
plus_intro :: Intro -> Intro -> Intro
plus_intro Zero      t         = t
plus_intro t         Zero      = t
plus_intro (Succ t1) t2        = Succ (plus_intro t1 t2)
plus_intro t1        (Succ t2) = Succ (plus_intro t1 t2)
plus_intro _         _         = error "No implementation of `(+)' for `Intro'"

times_intro :: Intro -> Intro -> Intro
times_intro Zero      t         = Zero
times_intro t         Zero      = Zero
times_intro (Succ t1) t2        = plus_intro t2 $ times_intro t1 t2
times_intro t1        (Succ t2) = plus_intro t1 $ times_intro t1 t2
times_intro _         _         = error "No implementation of `(*)' for `Intro'"

instance Num Intro where
  fromInteger = num_to_intro
  (+) = plus_intro
  (*) = times_intro
  abs = id
  signum = \ _ -> Succ Zero
  negate = error "No implementation of `negate' for `Term'"

-- Boolean instance
bnot_intro :: Intro -> Intro
bnot_intro TT = FF
bnot_intro FF = TT
bnot_intro _ = error "No implementation of `bnot'"

band_intro :: Intro -> Intro -> Intro
band_intro TT t = t
band_intro FF _ = FF
band_intro t TT = t
band_intro _ FF = FF
band_intro _ _  = error "No implementation of `(&&&)'"

instance Boolean Intro where 
  true  = TT
  bnot  = bnot_intro
  (&&&) = band_intro

-- Embedding lists and pairs 
list_to_intro :: (a -> Intro) -> [a] -> Intro
list_to_intro f [] = Nil
list_to_intro f (x:xs) = Cons (f x) (list_to_intro f xs)

pair_to_intro :: (a -> Intro) -> (b -> Intro) -> (a,b) -> Intro
pair_to_intro f g (x,y) = Pair (f x) (g y)

------------
-- TYPING --
------------

-- maybe_type_eq :: TType -> TType -> Maybe TType
-- maybe_type_eq a1 a2 = if a1 == a2 then Just a1 else Nothing

-- tm_to_type :: Term -> Maybe TType
-- tm_to_type (Var _ a)      = return a
-- tm_to_type TT             = Just TBool
-- tm_to_type FF             = Just TBool
-- tm_to_type (TIf t1 t2 t3) = do
--     t1_type <- tm_to_type t1
--     _       <- maybe_type_eq t1_type TBool
--     t2_type <- tm_to_type t2
--     t3_type <- tm_to_type t3
--     a       <- maybe_type_eq t2_type t3_type
--     return a
-- tm_to_type Zero           = Just TNat
-- tm_to_type (Succ t)       = do
--     t_type  <- tm_to_type t
--     a       <- maybe_type_eq t_type TNat
--     return a
-- tm_to_type (NCase t1 t2 x t3) = do
--     t1_type <- tm_to_type t1
--     _       <- maybe_type_eq t1_type TNat
--     t2_type <- tm_to_type t2
--     t3_type <- tm_to_type t3
--     a       <- maybe_type_eq t2_type t3_type
--     return a
-- tm_to_type (Nil a)        = return $ TList a
-- tm_to_type (Cons t1 t2)     = do
--     t1_type <- tm_to_type t1
--     t2_type <- tm_to_type t2
--     a       <- maybe_type_eq t1_type t2_type
--     return a
-- tm_to_type (LCase t1 t2 x xs t3) = 
--     case tm_to_type t1 of
--     Just (TList a) -> do
--         t2_type <- tm_to_type t2
--         t3_type <- tm_to_type t3
--         a       <- maybe_type_eq t2_type t3_type
--         return a
--     _ -> Nothing

--------------------
-- FREE VARIABLES --
--------------------

free_intro :: Intro -> ([String],[String])

free_intro (Var x a) = ([x],[])
free_intro (App f t) = 
  let (ls1,ls2) = free_intro t in
  (ls1,f:ls2)
free_intro TT    = ([],[])
free_intro FF    = ([],[])
free_intro Zero  = ([],[])
free_intro (Succ t) = free_intro t
free_intro Nil = ([],[])
free_intro (Cons t1 t2) = 
  let (ls1,ls2) = free_intro t1 in
  let (ls1',ls2') = free_intro t2 in
  (ls1++ls1',ls2++ls2')
free_intro (Pair t1 t2) = 
  let (ls1,ls2) = free_intro t1 in
  let (ls1',ls2') = free_intro t2 in
  (ls1++ls1',ls2++ls2')

